package com.example.retotwitter

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.modifier.modifierLocalOf
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.green

@Composable
fun tuit() {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color(0XFF171c26)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        Body(
            Modifier
                .padding(10.dp)
        )
        Divider(modifier = Modifier.fillMaxWidth().background(Color(0XFFB5B5B5)).height(1.dp))
    }


}

@Composable
fun Body(modifier: Modifier) {
    Row(
        modifier
            .fillMaxWidth()
            .padding(15.dp)
    ) {
        ProfileImage()
        BodyTuit()
    }
}

@Composable
fun ProfileImage() {
    Image(
        painter = painterResource(id = R.drawable.profile),
        contentDescription = "profile image",
        modifier = Modifier
            .size(60.dp)
            .clip(CircleShape),
        alignment = Alignment.TopCenter
    )
}

@Composable
fun BodyTuit() {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 20.dp)
    ) {
        Header(modifier = Modifier.fillMaxWidth())
        TextTuit()
        ImageTuit(modifier = Modifier.fillMaxWidth())
        FooterTuit(modifier = Modifier.fillMaxWidth())
    }
}

@Composable
fun Header(modifier: Modifier) {
    Row(modifier = modifier) {
        Text(
            text = "Duver",
            modifier = Modifier.padding(end = 5.dp),
            fontSize = 16.sp,
            color = Color.White,
            fontWeight = FontWeight.Bold
        )
        Text(
            text = "@DuverMelo",
            modifier = Modifier.padding(end = 5.dp),
            fontWeight = FontWeight.Bold,
            color = Color(0XFFB5B5B5),
            fontSize = 16.sp
        )
        Text(
            text = "4h",
            modifier = Modifier.padding(end = 5.dp),
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = Color(0XFFB5B5B5)
        )
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            painter = painterResource(id = R.drawable.ic_dots),
            contentDescription = "options logo",
            tint = Color.White
        )
    }
}

@Composable
fun TextTuit() {
    Text(
        text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et feugiat magna, laoreet fringilla magna. Phasellus ut neque ac nunc scelerisque auctor sit amet vel magna. Maecenas accumsan nulla sit amet velit tristique tristique. Fusce pellentesque pulvinar cursus. Fusce tellus erat, rhoncus congue bibendum vitae, efficitur vel tortor.",
        fontSize = 16.sp,
        color = Color.White
    )
}

@Composable
fun ImageTuit(modifier: Modifier) {
    Image(
        painter = painterResource(id = R.drawable.profile),
        contentDescription = "Image tuit",
        modifier = modifier
            .clip(
                RoundedCornerShape(20)
            )
            .padding(top = 10.dp, bottom = 10.dp)
    )
}

@Composable
fun FooterTuit(modifier: Modifier) {

    Row(modifier = modifier, horizontalArrangement = Arrangement.Start) {
        TuitComment(Modifier.weight(1f))
        ReTuit(Modifier.weight(1f))
        LikeTuit(Modifier.weight(1f))
    }

}

@Composable
fun TuitComment(modifier: Modifier) {
    var tuitTotal by remember {
        mutableStateOf(1)
    }
    var statusTuit by remember {
        mutableStateOf(false)
    }
    Row(modifier, verticalAlignment = Alignment.CenterVertically) {
        IconButton(
            onClick = {
                statusTuit = !statusTuit
                tuitTotal = if (statusTuit) 2 else 1
            }
        ) {
            if (statusTuit) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_chat_filled),
                    contentDescription = "icon chat", tint = Color(0XFFB5B5B5)
                )
            } else {
                Icon(
                    painter = painterResource(id = R.drawable.ic_chat),
                    contentDescription = "icon chat", tint = Color(0XFFB5B5B5)
                )
            }
        }
        Text(text = "$tuitTotal",  color = Color(0XFFB5B5B5))
    }
}

@Composable
fun ReTuit(modifier: Modifier) {
    var reTuitTotal by remember {
        mutableStateOf(1)
    }
    var reTuitStatus by remember {
        mutableStateOf(false)
    }
    Row(modifier, verticalAlignment = Alignment.CenterVertically) {
        IconButton(
            onClick = {
                reTuitStatus = !reTuitStatus
                reTuitTotal = if (reTuitStatus) 2 else 1
            }
        ) {
            if (reTuitStatus) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_rt),
                    contentDescription = "retuit icon",
                    tint = Color.Green
                )
            } else {
                Icon(
                    painter = painterResource(id = R.drawable.ic_rt),
                    contentDescription = "retuit icon",
                    tint = Color(0XFFB5B5B5)
                )
            }
        }
        Text(text = "$reTuitTotal", color = Color(0XFFB5B5B5))
    }
}

@Composable
fun LikeTuit(modifier: Modifier) {
    var likeTuitTotal by remember {
        mutableStateOf(1)
    }
    var likeTuitStatus by remember {
        mutableStateOf(false)
    }
    Row(modifier, verticalAlignment = Alignment.CenterVertically) {
        IconButton(
            onClick = {
                likeTuitStatus = !likeTuitStatus
                likeTuitTotal = if (likeTuitStatus) 2 else 1
            }
        ) {
            if (likeTuitStatus) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_like_filled),
                    contentDescription = "retuit icon",
                    tint = Color.Red
                )
            } else {
                Icon(
                    painter = painterResource(id = R.drawable.ic_like),
                    contentDescription = "retuit icon",
                    tint = Color(0XFFB5B5B5)
                )
            }
        }
        Text(text = "$likeTuitTotal",  color = Color(0XFFB5B5B5))
    }

}